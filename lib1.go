package mathlib1

// Add two integer numbers
func Add(a int, b int) int {

	return a + b
}

// Version returns version string for debugging
func Version() string {
	return "v0.1.0"
}
